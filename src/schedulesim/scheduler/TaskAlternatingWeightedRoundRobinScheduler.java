package schedulesim.scheduler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import schedulesim.Consumer;
import schedulesim.ConsumingEntity;
import schedulesim.Scheduler;
import schedulesim.Task;
import schedulesim.TaskMaxFirstComparator;
import schedulesim.TaskMinFirstComparator;

/**
 * This work is licensed under the Creative Commons Attribution 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative
 * Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 *
 * @author paul moggridge (paulmogs398@gmail.com)
 */
public class TaskAlternatingWeightedRoundRobinScheduler extends Scheduler {

    private int index;
    private Random random;

    public TaskAlternatingWeightedRoundRobinScheduler() {
        super();
        index = 0;
        random = new Random();
    }

    @Override
    public void reset(){
      super.reset();
      index = 0;
    }
    
    @Override
    public void step() {
        super.step();

        if (super.getChildren().size() > 0) {
            
            //
            // Group the tasks into two groups, big and small
            //
            
            // Create lists for big and small tasks
            ArrayList<Task> bigTasks = new ArrayList<>();
            ArrayList<Task> smallTasks = new ArrayList<>();
            
            // Work out the halfway point this will
            int halfwayPoint = super.getWaitingTasks().size() / 2;
            
            // Sort the task min first, smallest tasks first
            Collections.sort(super.getWaitingTasks(), new TaskMinFirstComparator());
            
            // Place tasks in either the big or small list
            int taskIndex = 0;
            while(super.getWaitingTasks().size()>0){
                if(taskIndex < halfwayPoint){
                    smallTasks.add(super.getWaitingTasks().remove(0));
                }else{
                    bigTasks.add(super.getWaitingTasks().remove(0));
                }
                taskIndex++;
            }
            
            
            //
            // Apply weighted round robin alternating between the big and small
            // tasks.
            //
            
            // Find fastest child, used later for working out the weights
            ConsumingEntity fastestChild = null;
            for (ConsumingEntity child : super.getChildren()) {
                if ((fastestChild == null)
                        || fastestChild.getUnitsPerStep() < child.getUnitsPerStep()) {
                    fastestChild = child;
                }
            }
            
            // Create array for storing what each consumer last had i.e. big or small
            Boolean[] lastTaskWasSmall = new Boolean[super.getChildren().size()];            
            while (smallTasks.size() > 0 || bigTasks.size() > 0) {
                // Use round robin to select a child
                ConsumingEntity child = super.getChildren().get(index % super.getChildren().size());
                // Work out the weight for this machine based on its UPS (speed)
                double wieght = ((double) child.getUnitsPerStep()) / ((double) fastestChild.getUnitsPerStep());
                // Get a random between 0 - 1 is our number lower than the wieght if so assign task
                if (random.nextDouble() < wieght) {
                    
                    // Alternate between big and small tasks (if possible)
                    if(smallTasks.size() > 0 && bigTasks.size() > 0){
                        
                        // if consumers last task was big give it a small task and vice versa
                        if(lastTaskWasSmall[index % super.getChildren().size()] != null){
                            // Was the last task they had small
                            if(lastTaskWasSmall[index % super.getChildren().size()]){
                                // Give a big task
                                child.submitTask(bigTasks.remove(0));
                                lastTaskWasSmall[index % super.getChildren().size()] = false;
                            }else{
                                // Give a small task
                                child.submitTask(smallTasks.remove(0));
                                lastTaskWasSmall[index % super.getChildren().size()] = true;
                            }
                        } else {
                            // First time assigning to this consumer select big or small
                            // using whether the index is odd or even
                            if(index % 2 == 0){
                                // index is even, give a small tasks
                                child.submitTask(smallTasks.remove(0));
                                lastTaskWasSmall[index % super.getChildren().size()] = true;
                            }else{
                                // index is odd, give a big task
                                child.submitTask(bigTasks.remove(0));
                                lastTaskWasSmall[index % super.getChildren().size()] = false;
                            }
                        }
                        
                    }else{

                        // We have no choice, i.e one of the list has no tasks
                        if(smallTasks.size() > 0){
                            // Only small task avaiable
                            child.submitTask(smallTasks.remove(0));
                            lastTaskWasSmall[index % super.getChildren().size()] = true; // un-necessary?
                        } else {
                            // Only a big task avaiable
                            child.submitTask(bigTasks.remove(0));
                            lastTaskWasSmall[index % super.getChildren().size()] = false; // un-necessary?
                        }
                        
                    }
                    
                    // Update gif, if we are scheduling to a Consumer
                    if(child instanceof Consumer){
                        super.updateGif();
                    }
                }
                
                index++;
            }
        }
    }
}

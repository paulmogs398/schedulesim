package schedulesim.scheduler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import schedulesim.Consumer;
import schedulesim.ConsumingEntity;
import schedulesim.ConsumingEntityMinFirstComparator;
import schedulesim.Task;
import schedulesim.TaskMaxFirstComparator;
import schedulesim.Log;
import schedulesim.Scheduler;
import schedulesim.TaskMinFirstComparator;

/**
 * This work is licensed under the Creative Commons Attribution 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative
 * Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * @author paul moggridge (paulmogs398@gmail.com)
 */
public class ClusteringMinminMaxminFastTrackScheduler extends Scheduler {

  private final int CENTROIDS = 2;
  private float threshold;

  public ClusteringMinminMaxminFastTrackScheduler() {
    this(0.1f);
  }

  public ClusteringMinminMaxminFastTrackScheduler(float threshold) {
    super();
    this.threshold = threshold;
  }

  @Override
  public void step() {
    super.step();
    if (super.getChildren().size() > 0 && super.getWaitingTasks().size() > 0) {

      // This will store the delays on children below.
      HashMap<ConsumingEntity, Double> childDelay = new HashMap<>();

      // Find any exsisting delay on child from previous waves
      for (ConsumingEntity child : super.getChildren()) {

        // Create entry for child
        childDelay.put(child, child.getDelay());
      }

      // Is it worth having a fast track?
      // Calculate standard deviation and compare with range
      int range = getRange(getMax(), getMin());
      double mean = getMean();
      double variance = getVariance(mean);
      double stdev = getStdev(variance);
      
      if (range != 0 && (stdev / range) > threshold) {
        // Use a fast track
        ArrayList<Task> normalTrackTasks = new ArrayList<>();
        ArrayList<Task> fastTrackTasks = new ArrayList<>();
        int taskUnitsInNormalTrack = 0; // The amount of work in the normal track
        int taskUnitsInFastTrack = 0;  // The amount of work in the fast track

        Cluster[] clusters = computeClusters();

        // Select lowest cluster
        Cluster smallestTasksCluster = clusters[0];
        for (Cluster cluster : clusters) {
          if (smallestTasksCluster.getAvgTaskUnits() > cluster.getAvgTaskUnits()) {
            smallestTasksCluster = cluster;
          }
        }

        // Assign the tasks in the smallest cluster to the fast track
        // assign the tasks in the other clusters to the normal track
        // Using the sorted task list (biggest length first), put the biggest tasks into the normal track,
        while (super.getWaitingTasks().size() > 0) {
          Task task = super.getWaitingTasks().get(0);
          
          if (smallestTasksCluster.hasTask(task)) {
            taskUnitsInFastTrack += super.getWaitingTasks().get(0).getRemaingUnits();
            fastTrackTasks.add(super.getWaitingTasks().remove(0));
          } else {
            taskUnitsInNormalTrack += super.getWaitingTasks().get(0).getRemaingUnits();
            normalTrackTasks.add(super.getWaitingTasks().remove(0));
          }
        }

        // Count total Unit Per Step
        int totalUPS = 0;
        for (ConsumingEntity child : super.getChildren()) {
          totalUPS += child.getUnitsPerStep();
        }

        // Based on the distribution of fast trackable task units to normal task units
        // calculate percentages of consumer units per step for the normal and fast tracks.
        double consumerUPSToTaskUPS = totalUPS / ((double) taskUnitsInNormalTrack + (double) taskUnitsInFastTrack);
        // Below is used later to decide how many UPS to put in the fast track
        double fastTrackTargetMips = consumerUPSToTaskUPS * taskUnitsInFastTrack;

        // Sort the child fastest (min. time) first (for building the FastTrack) i.e.
        // The ConsumingEntities with biggest UPS go first and thus into the fast track
        Collections.sort(super.getChildren(), new ConsumingEntityMinFirstComparator());

        // Assign VMs to fast or normal track
        ArrayList<ConsumingEntity> copyChildren = new ArrayList<>(super.getChildren());
        ArrayList<ConsumingEntity> fastTrack = createFastTrack(copyChildren, fastTrackTargetMips);
        ArrayList<ConsumingEntity> normalTrack = createNormalTrack(copyChildren);

        if (copyChildren.size() > 0) {
          Log.println(copyChildren.size() + " Consuming Entities where not assigned a track.");
        }

        // MaxMin tasks onto FastTrack
        //Collections.sort(fastTrackTasks , new TaskMaxFirstComparator());
        //Minmin tasks onto the Fast Track (rather than Maxmin)
        // Sort the task min first, smallest tasks first
        Collections.sort(fastTrackTasks, new TaskMinFirstComparator());

        while (fastTrackTasks.size() > 0) {
          Task task = fastTrackTasks.get(0);

          // Which ConsumingEntity can finish it first? i.e in the min. time,
          // takes into account the UnitPerStep of the ConsumingEntity and tasks
          // already scheduled to it.
          double minChildDelay = 0.0;
          ConsumingEntity minChild = null;

          // Which child can complete task first
          for (ConsumingEntity child : fastTrack) {
            double taskMakespanWithChildDelay = ((double) task.getRemaingUnits() / (double) child.getUnitsPerStep()) + childDelay.get(child);
            // Can this child finish the task faster
            if (taskMakespanWithChildDelay < minChildDelay || minChild == null) {
              minChild = child;
              minChildDelay = taskMakespanWithChildDelay;
            }
          }

          // Update child delays map
          childDelay.put(minChild, minChildDelay);

          // Submit task to child
          minChild.submitTask(fastTrackTasks.remove(0));
          // Update gif, if we are scheduling to a Consumer
          if (minChild instanceof Consumer) {
            super.updateGif();
          }
        }

        // MaxMin tasks onto NormalTrack
        Collections.sort(normalTrackTasks, new TaskMaxFirstComparator());

        while (normalTrackTasks.size() > 0) {
          Task task = normalTrackTasks.get(0);

          // Which ConsumingEntity can finish it first? i.e in the min. time,
          // takes into account the UnitPerStep of the ConsumingEntity and tasks
          // already scheduled to it.
          double minChildDelay = 0.0;
          ConsumingEntity minChild = null;

          // Which child can complete task first
          for (ConsumingEntity child : normalTrack) {
            double taskMakespanWithChildDelay = ((double) task.getRemaingUnits() / (double) child.getUnitsPerStep()) + childDelay.get(child);
            // Can this child finish the task faster
            if (taskMakespanWithChildDelay < minChildDelay || minChild == null) {
              minChild = child;
              minChildDelay = taskMakespanWithChildDelay;
            }
          }

          // Update child delays map
          childDelay.put(minChild, minChildDelay);

          // Submit task to child
          minChild.submitTask(normalTrackTasks.remove(0));

          // Update gif, if we are scheduling to a Consumer
          if (minChild instanceof Consumer) {
            super.updateGif();
          }
        }

      } else {
        // Don't use a fast track
        // Just use Maxmin

        // Sort the task max first, biggest tasks first
        Collections.sort(super.getWaitingTasks(), new TaskMaxFirstComparator());

        while (super.getWaitingTasks().size() > 0) {
          Task task = super.getWaitingTasks().get(0);

          // Which ConsumingEntity can finish it first? i.e in the min. time,
          // takes into account the UnitPerStep of the ConsumingEntity and tasks
          // already scheduled to it.
          double minChildDelay = 0.0;
          ConsumingEntity minChild = null;

          // Which child can complete task first
          for (ConsumingEntity child : super.getChildren()) {
            double taskMakespanWithChildDelay = ((double) task.getRemaingUnits() / (double) child.getUnitsPerStep()) + childDelay.get(child);
            // Can this child finish the task faster
            if (taskMakespanWithChildDelay < minChildDelay || minChild == null) {
              minChild = child;
              minChildDelay = taskMakespanWithChildDelay;
            }
          }

          // Update child delays map
          childDelay.put(minChild, minChildDelay);

          // Submit task to child
          minChild.submitTask(super.getWaitingTasks().remove(0));
          // Update gif, if we are scheduling to a Consumer
          if (minChild instanceof Consumer) {
            super.updateGif();
          }
        }
      }
    }
  }

  public ArrayList<ConsumingEntity> createFastTrack(ArrayList<ConsumingEntity> allConsumingEntities, double fastTrackTargetUPS) {
    ArrayList<ConsumingEntity> fastTrack = new ArrayList<>();
    int fastTrackUPS = 0;

    // Loop over biggest ConsumingEntities first, if it fits in the fast track place in there
    while ((fastTrackTargetUPS - fastTrackUPS) > allConsumingEntities.get(0).getUnitsPerStep()) {
      // Add VM to fast track
      fastTrackUPS += allConsumingEntities.get(0).getUnitsPerStep();
      fastTrack.add(allConsumingEntities.remove(0));
    }

    // Check we have at least one VM in fast track
    if (fastTrack.size() <= 0) {
      // If we not, add one smallest VM
      fastTrackUPS += allConsumingEntities.get(allConsumingEntities.size() - 1).getUnitsPerStep();
      fastTrack.add(allConsumingEntities.remove(allConsumingEntities.size() - 1));
    }

    return fastTrack;
  }

  public ArrayList<ConsumingEntity> createNormalTrack(ArrayList<ConsumingEntity> remainingConsumingEntities) {

    // Create the NormalTrack and FastTrack as close to the target MIPS as possible
    ArrayList<ConsumingEntity> normalTrack = new ArrayList<>();

    // Add all remaining VMs
    while (remainingConsumingEntities.size() > 0) {
      normalTrack.add(remainingConsumingEntities.remove(0));
    }

    return normalTrack;
  }

  private int getMax() {
    Task max = super.getWaitingTasks().get(0);
    for (Task task : super.getWaitingTasks()) {
      if (max.getRemaingUnits() < task.getRemaingUnits()) {
        max = task;
      }
    }
    return max.getRemaingUnits();
  }

  private int getMin() {
    Task min = super.getWaitingTasks().get(0);
    for (Task task : super.getWaitingTasks()) {
      if (min.getRemaingUnits() > task.getRemaingUnits()) {
        min = task;
      }
    }
    return min.getRemaingUnits();
  }

  private int getRange(int max, int min) {
    return max - min;
  }

  private double getMean() {
    int total = 0;
    for (Task task : super.getWaitingTasks()) {
      total += task.getRemaingUnits();
    }
    return total / super.getWaitingTasks().size();
  }

  private double getVariance(double mean) {
    int variance = 0;
    for (Task task : super.getWaitingTasks()) {
      // Square the difference from the mean
      variance += ((task.getRemaingUnits() - mean)*(task.getRemaingUnits() - mean));
    }
    return variance / super.getWaitingTasks().size();
  }

  private double getStdev(double variance) {
    return Math.sqrt(variance);
  }

  private int getDistance(int a, int b) {
    return Math.abs(a - b);
  }

  public Cluster[] computeClusters() {
    Cluster[] clusters = new Cluster[CENTROIDS];

    // Create clusters with randomly placed centroids
    Random random = new Random();
    int max = getMax();
    int min = getMin();
    int range = getRange(max, min);
    for (int i = 0; i < CENTROIDS; i++) {
      int centroid = random.nextInt(range + min);
      clusters[i] = new Cluster(centroid);
    }

    // Assign points to their nearest centroid
    for (Task task : super.getWaitingTasks()) {
      // Find cluster with nearest centroid
      int nearest = getDistance(task.getRemaingUnits(), clusters[0].getCentroid());
      Cluster nearestCluster = clusters[0];
      for (Cluster cluster : clusters) {
        int distance = getDistance(task.getRemaingUnits(), cluster.getCentroid());
        if (nearest > distance) {
          nearest = distance;
          nearestCluster = cluster;
        }
      }
      // Add point to Cluster
      nearestCluster.addValue(task);
    }

    // Run until no points are re-assigned
    boolean reassignment = true;
    while (reassignment) {
      reassignment = false;

      // Clusters compute their new centroids
      for (Cluster cluster : clusters) {
        cluster.computeNewCentroid();
      }

      // Try assigning points
      for (Task task : super.getWaitingTasks()) {
        // Find cluster with nearest centroid
        int nearest = getDistance(task.getRemaingUnits(), clusters[0].getCentroid());
        Cluster nearestCluster = clusters[0];
        for (Cluster cluster : clusters) {
          int distance = getDistance(task.getRemaingUnits(), cluster.getCentroid());
          if (nearest > distance) {
            nearest = distance;
            nearestCluster = cluster;
          }
        }

        // Is this not the cluster it already belonged to
        if (!nearestCluster.hasTask(task)) {
          // Remove point from it's previous cluster
          for (Cluster cluster : clusters) {
            cluster.removeTask(task);
          }
          // Add point to new nearest Cluster
          nearestCluster.addValue(task);
          reassignment = true;
        }
      }
    }

    return clusters;
  }

  /**
   *
   * @author paul
   */
  public class Cluster {

    private int centroid;
    private ArrayList<Task> tasks;

    public Cluster(int centroid) {
      this.centroid = centroid;
      this.tasks = new ArrayList<>();
    }

    public void addValue(Task point) {
      tasks.add(point);
    }

    public int getCentroid() {
      return centroid;
    }

    public ArrayList<Task> getTasks() {
      return tasks;
    }

    public void removeTask(Task task) {
      tasks.remove(task);
    }

    public void computeNewCentroid() {
      if(tasks.size()>0){
        int total = 0;
        for (Task task : tasks) {
          total += task.getRemaingUnits();
        }
        centroid = total / tasks.size();
      }
    }

    public boolean hasTask(Task task) {
      return tasks.contains(task);
    }

    public double getAvgTaskUnits() {
      if(tasks.size()>0){
        double totalClusterUnits = 0;
        for (Task task : tasks) {
          totalClusterUnits += task.getRemaingUnits();
        }
        return totalClusterUnits / tasks.size();
      }else{
        return 0;
      }
    }

    @Override
    public String toString() {
      return "Cluster{" + "centroid=" + centroid + ", tasks.size=" + tasks.size() + '}';
    }
  }
}

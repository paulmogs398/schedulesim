package schedulesim.scheduler;

import java.util.Random;
import schedulesim.Consumer;
import schedulesim.Scheduler;
import schedulesim.ConsumingEntity;
import schedulesim.Task;

/**
 * This work is licensed under the Creative Commons Attribution 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative
 * Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * @author paul moggridge (paulmogs398@gmail.com)
 */
public class ShoppingCompletionTimeScheduler extends Scheduler {

  private int optionCount;
  private Random random;

  public ShoppingCompletionTimeScheduler() {
    this(8);
  }

  public ShoppingCompletionTimeScheduler(int optionCount) {
    super();
    this.optionCount = optionCount;
    random = new Random();
  }

  @Override
  public void step() {
    super.step();
    if (super.getChildren().size() > 0) {
      while (super.getWaitingTasks().size() > 0) {

        // Make sure there is enough consuming entities
        if (optionCount > super.getChildren().size()) {
          optionCount = super.getChildren().size();
        }

        Task task = super.getWaitingTasks().remove(0);
        
        // Randomly choose X consuming entities
        // and find the child entity that can give the
        // best completion time for our task
        ConsumingEntity minCompTimeEntity = null;
        double minimumCompletionTime = 0;
        for (int i = 0; i < optionCount; i++) {
          ConsumingEntity child = super.getChildren().get(random.nextInt(super.getChildren().size()));
          
          // Calculate completion time
          double completionTime = ((double)task.getRemaingUnits() / (double)child.getUnitsPerStep()) + child.getDelay();

          // Is this sooner than our current best option?
          if (minCompTimeEntity == null
            || minimumCompletionTime > completionTime) {
            minCompTimeEntity = child;
            minimumCompletionTime = completionTime;
          }
        }

        // Assign task to min of randomly selected entities
        minCompTimeEntity.submitTask(task);

        // Update gif, if we are scheduling to a Consumer
        if (minCompTimeEntity instanceof Consumer) {
          super.updateGif();
        }
      }
    }
  }

}

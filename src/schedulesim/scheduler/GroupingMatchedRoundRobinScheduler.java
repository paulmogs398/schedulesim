package schedulesim.scheduler;

import java.util.ArrayList;

import schedulesim.Consumer;
import schedulesim.ConsumingEntity;
import schedulesim.Scheduler;
import schedulesim.Task;


/**
 * This work is licensed under the Creative Commons Attribution 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative
 * Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 *
 * @author paul moggridge (paulmogs398@gmail.com)
 */
public class GroupingMatchedRoundRobinScheduler extends Scheduler {

    private int groupCount;

    public GroupingMatchedRoundRobinScheduler(int groupCount) {
        super();
        this.groupCount = groupCount;
    }

    @Override
    public void reset() {
        super.reset();
    }

    @Override
    public void step() {
        super.step();
        
        if (super.getChildren().size() > 0 && super.getWaitingTasks().size() > 0) {
          
            // Work out the total UPS of all the connected consumers
            int totalUPS = 0;
            for(int c = 0; c < super.getChildren().size(); c++){
              // Sum the units each consumer has or each scheduler has below it
              totalUPS += super.getChildren().get(c).getUnitsPerStep();
            }
            
            // Work out the total Units waiting tasks
            int totalUnits = 0;
            for(int t = 0; t < super.getWaitingTasks().size(); t++){
              // sum up the total amount of work to be completed
              totalUnits += super.getWaitingTasks().get(t).getRemaingUnits();
            }
            
            // Work out group depth, ceil as we can't use decimal point of tasks
            int tasksPerGroup = (int)Math.ceil(super.getWaitingTasks().size() / ((double)groupCount));
            // The consumers per group are dynamically decided
            
            // Based on the sizes of the groups of tasks, create the groups of consumers to match
            // The list of consumers to match this groups tasks
            ArrayList<ArrayList<Task>> taskGroup = new ArrayList<>();
            ArrayList<ArrayList<ConsumingEntity>> consumerGroup = new ArrayList<>();
            for (int g = 0, ci = 0; g < groupCount && !(super.getWaitingTasks().isEmpty()); g++) {
                            
              // Count up the amount of work in the task group
              int taskGroupUnits = 0;
              taskGroup.add(new ArrayList<>());
              
              // Assign task to the group
              for(int ti = 0; ti < tasksPerGroup || ((g * tasksPerGroup) + ti) > super.getWaitingTasks().size(); ti++){
                 taskGroupUnits += super.getWaitingTasks().get((g * tasksPerGroup) + ti).getRemaingUnits();
                 taskGroup.get(g).add(super.getWaitingTasks().get((g * tasksPerGroup) + ti));
              }
              
              // What is this a percentage of the total units
              double unitsPercentage = ((double)taskGroupUnits) / ((double)totalUnits);
              
              // What is the above percentage in consumers / UPS
              int consumerGroupTargetUPS = (int)Math.ceil(((double)totalUPS) * ((double)unitsPercentage));
              
              // Create group of consumers
              int consumerGroupUPS = 0;
              consumerGroup.add(new ArrayList<>());
              
              // Assign conusmers using ci consumer index to make sure we don't
              // assign a conusmer to more than  one group
              while(consumerGroupUPS < consumerGroupTargetUPS && ci < super.getChildren().size()){
                consumerGroupUPS += super.getChildren().get(ci).getUnitsPerStep();
                consumerGroup.get(g).add(super.getChildren().get(ci));
                ci++;
              }
            }
            
            // Using round robin assign the groups of task to their corresponding
            // consumer group.
            for(int g = 0; g < groupCount; g++){
              
              // Select the next consumer in the group of consumers
              int ci = 0;
              while(!taskGroup.get(g).isEmpty()){
                ConsumingEntity child = consumerGroup.get(g).get(ci++ % consumerGroup.get(g).size());
                child.submitTask(taskGroup.get(g).remove(0));
                super.getWaitingTasks().remove(0);
                
                // Update gif, if we are scheduling to a Consumer
                if (child instanceof Consumer) {
                    super.updateGif();                
                }
              }
            }
            
        }

    }
}

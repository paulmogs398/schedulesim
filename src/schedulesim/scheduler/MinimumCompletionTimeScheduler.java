package schedulesim.scheduler;

import java.util.Random;
import schedulesim.Consumer;
import schedulesim.ConsumingEntity;
import schedulesim.Scheduler;
import schedulesim.Task;

/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 * 
 * @author paul moggridge (paulmogs398@gmail.com)
 */
public class MinimumCompletionTimeScheduler extends Scheduler {

  private Random random;

  public MinimumCompletionTimeScheduler(){
    super();
    random = new Random();
  }

  @Override
  public void step() {
    super.step();
    if (super.getChildren().size() > 0) {
        
      while(super.getWaitingTasks().size() > 0){
        
          Task task = super.getWaitingTasks().remove(0);
        
          // Scan through consumers to find the child entity that can give the
          // best completion time
          ConsumingEntity minCompTimeEntity = null;
          double minimumCompletionTime = 0;
          for(ConsumingEntity child : super.getChildren()){
              
              // Calculate completion time for this child with this task
              double completionTime = ((double)task.getRemaingUnits() / (double)child.getUnitsPerStep()) + child.getDelay();
              
              // Is this sooner than our current best option?
              if(minCompTimeEntity == null ||
                    minimumCompletionTime > completionTime){
                  minCompTimeEntity = child;
                  minimumCompletionTime = completionTime;
              }
          }
          
        minCompTimeEntity.submitTask(task);
        
        // Update gif, if we are scheduling to a Consumer
        if(minCompTimeEntity instanceof Consumer){
            super.updateGif();
        }
      }
    }
  }

}

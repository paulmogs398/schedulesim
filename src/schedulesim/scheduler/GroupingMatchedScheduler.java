package schedulesim.scheduler;

import java.util.Collections;
import schedulesim.Consumer;
import schedulesim.ConsumingEntity;
import schedulesim.ConsumingEntityMaxFirstComparator;
import schedulesim.Scheduler;
import schedulesim.Task;

/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 * 
 * 
 * @author paul moggridge (paulmogs398@gmail.com)
 */
public class GroupingMatchedScheduler extends Scheduler {

  public GroupingMatchedScheduler() {
    super();
  }

  @Override
  public void reset(){
    super.reset();
  }
  
  @Override
  public void step() {
    super.step();
    
    if (super.getChildren().size() > 0) {
        
        // Work out the total UPS of all the connected consumers
        int totalUPS = 0;
        for(int c = 0; c < super.getChildren().size(); c++){
          // Sum the units each consumer has or each scheduler has below it
          totalUPS += super.getChildren().get(c).getUnitsPerStep();
        }

        // Work out the total Units waiting tasks
        int totalUnits = 0;
        for(int t = 0; t < super.getWaitingTasks().size(); t++){
          // sum up the total amount of work to be completed
          totalUnits += super.getWaitingTasks().get(t).getRemaingUnits();
        }
        
        // process each child
        for(int c = 0; c < super.getChildren().size(); c++){            
            
            // The child we are currently scheduling to
            ConsumingEntity child = super.getChildren().get(c);
            
            // What percentage of total UPS, does this entity represent
            double percentageOfUPS = super.getChildren().get(c).getUnitsPerStep() / totalUPS;
            
            // Submit a balanced amount of tasks, i.e. if this consuming entity
            // represents 30% of the total UPS, then allocate 30% of the tasks
            double unitsSoFar = 0;
            double targetUnits = percentageOfUPS * totalUnits;
            while(unitsSoFar <= targetUnits && super.getWaitingTasks().size() > 0){
                
                Task task = super.getWaitingTasks().remove(0);                
                unitsSoFar += task.getRemaingUnits();
                // submit the task
                child.submitTask(task);
                // Update gif, if we are scheduling to a Consumer
                if (child instanceof Consumer) {
                    super.updateGif();
                }
            }
            
        }
    }
  }
}

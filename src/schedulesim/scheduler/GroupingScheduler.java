package schedulesim.scheduler;

import java.util.Collections;
import schedulesim.Consumer;
import schedulesim.ConsumingEntity;
import schedulesim.ConsumingEntityMaxFirstComparator;
import schedulesim.Scheduler;
import schedulesim.TaskMaxFirstComparator;
import schedulesim.TaskMinFirstComparator;

/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 * 
 * 
 * @author paul moggridge (paulmogs398@gmail.com)
 */
public class GroupingScheduler extends Scheduler {

  public GroupingScheduler() {
    super();
  }

  @Override
  public void reset(){
    super.reset();
  }
  
  @Override
  public void step() {
    super.step();
    if (super.getChildren().size() > 0) {
      
      // Sort the task max first, biggest tasks first
      Collections.sort(super.getWaitingTasks(), new TaskMaxFirstComparator());

      // The ConsumingEntities with lowest UPS first, thus slowest first
      Collections.sort(super.getChildren(), new ConsumingEntityMaxFirstComparator());
      
      // Work out group depth, ceil as we can't decimal point groups i.e. split resources or tasks
      // It is possible that there are more groups than tasks or consumers
      int tasksPerGroup = (int)Math.ceil(super.getWaitingTasks().size() / (super.getChildren().size()  * 1.0));
      
      // Process each group
      for (int g = 0; g < super.getChildren().size() && !(super.getWaitingTasks().isEmpty()); g++) {
          // Assign tasks to consumer for each group.
          // The smallest sized task group get sent to the slowest resource
          // The biggest sized task group get sent to the fasetst resource.
          for (int t = 0; t < tasksPerGroup && !(super.getWaitingTasks().isEmpty()); t++) {
              
              // Submit the task
              ConsumingEntity child = super.getChildren().get(g);
              child.submitTask(super.getWaitingTasks().remove(0));
              
              // Update gif, if we are scheduling to a Consumer
              if (child instanceof Consumer) {
                  super.updateGif();
              }
          }
      }
    }
  }
}

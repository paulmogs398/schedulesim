package schedulesim.scheduler;

import java.util.Collections;
import schedulesim.Consumer;
import schedulesim.ConsumingEntity;
import schedulesim.ConsumingEntityMaxFirstComparator;
import schedulesim.Scheduler;
import schedulesim.TaskMinFirstComparator;

/**
 * This work is licensed under the Creative Commons Attribution 4.0
 * International License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative
 * Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 *
 * @author paul moggridge (paulmogs398@gmail.com)
 */
public class GroupingRoundRobinScheduler extends Scheduler {

    private int groupCount;

    public GroupingRoundRobinScheduler(int groupCount) {
        super();
        this.groupCount = groupCount;
    }

    @Override
    public void reset() {
        super.reset();
    }

    @Override
    public void step() {
        super.step();

        if (super.getChildren().size() > 0) {

            // Sort the task min first, smallest tasks first
            Collections.sort(super.getWaitingTasks(), new TaskMinFirstComparator());

            // The ConsumingEntities with lowest UPS first, thus slowest first
            Collections.sort(super.getChildren(), new ConsumingEntityMaxFirstComparator());

            // Work out group depth, ceil as we can't decimal point groups i.e. split resources or tasks
            // It is possible that there are more groups than tasks or consumers
            int tasksPerGroup = (int)Math.ceil(super.getWaitingTasks().size() / (groupCount*1.0));
            int consumersPerGroup = (int)Math.floor(super.getChildren().size() / (groupCount*1.0));

            // Process each group
            for (int g = 0; g < groupCount && !(super.getWaitingTasks().isEmpty()); g++) {
                // Assign tasks to consumer for each group.
                // The smallest sized task group get round robined onto
                // the slowest resources.... The biggest sized task group
                // get round robined onto the fastest consumers.
                int index = 0;
                for (int t = 0; t < tasksPerGroup && !(super.getWaitingTasks().isEmpty()); t++) {
                    int consumerIndex = (consumersPerGroup * g) + (index++ % consumersPerGroup);
                    //if(super.getChildren().size()-1 < consumerIndex){
                    //    consumerIndex--;
                    //}
                    ConsumingEntity child = super.getChildren().get(consumerIndex);
                    child.submitTask(super.getWaitingTasks().remove(0));                    
                    // Update gif, if we are scheduling to a Consumer
                    if (child instanceof Consumer) {
                        super.updateGif();
                    }
                }
            }
        }

    }
}

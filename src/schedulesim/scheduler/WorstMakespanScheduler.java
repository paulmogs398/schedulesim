package schedulesim.scheduler;

import schedulesim.Consumer;
import schedulesim.ConsumingEntity;
import schedulesim.Scheduler;

/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 * 
 * 
 * @author paul moggridge (paulmogs398@gmail.com)
 */
public class WorstMakespanScheduler extends Scheduler {

  private int index;

  public WorstMakespanScheduler() {
    super();
    index = 0;
  }

  @Override
  public void reset(){
    super.reset();
    index = 0;
  }
  
  @Override
  public void step() {
    super.step();
    if (super.getChildren().size() > 0) {
        
        // Find the slowest consumer
        ConsumingEntity slowestChild = null;
        for(ConsumingEntity child : super.getChildren()){
            if(slowestChild == null || slowestChild.getUnitsPerStep() > child.getUnitsPerStep()){
                slowestChild = child;
            }
        }
        
        // Place all the jobs on it
        while(super.getWaitingTasks().size() > 0){
            slowestChild.submitTask(super.getWaitingTasks().remove(0));
            // Update gif, if we are scheduling to a Consumer
            if(slowestChild instanceof Consumer){
                super.updateGif();
            }
        }
    }
  }
}

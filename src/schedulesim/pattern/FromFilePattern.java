package schedulesim.pattern;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import schedulesim.MetataskPattern;
import schedulesim.Task;

/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 * 
 * @author paul moggridge (paulmogs398@gmail.com)
 */
public class FromFilePattern implements MetataskPattern {

  private final String path;

  public FromFilePattern(String path){
    this.path = path;
  }

  @Override
  public ArrayList<Task> generateMetatask() {
    ArrayList<Task> tasks = new ArrayList<>();
    
    try {
      File file = new File(path);
      Scanner scanner = new Scanner(file);
      do {
        String size = scanner.nextLine();

        tasks.add(new Task(Integer.parseInt(size)));

      } while (scanner.hasNext());

    } catch (Exception ex) {
      System.out.println("Error loading task file: " + ex.getMessage());
      System.exit(1);
    }
    
    
    return tasks;
  }

}

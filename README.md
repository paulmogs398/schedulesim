# README

ScheduleSim is an open source batch mode vector bin packing simulator implemented in Java.

For informationon how to make use of ScheduleSim check the wiki at https://bitbucket.org/paulmogs398/schedulesim/wiki/Home

If you have any problems, questions, comments or advice to improve this application please email paulmogs398@gmail.com  many thanks. Paul
